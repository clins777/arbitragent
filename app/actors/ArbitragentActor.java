package actors;

import akka.actor.AbstractActor;
import akka.actor.Props;
import com.google.common.collect.Lists;
import models.DepthStamp;
import models.GraphNode;
import play.Logger;
import play.libs.Json;
import scala.reflect.internal.Depth;
import services.ArbitrageService;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;

public class ArbitragentActor extends AbstractActor {

    // BNB/BTC
    // BNB/ETH
    // ETH/BTC

    private final ArbitrageService arbitrageService;
    private List<DepthStamp> testDepthStamps;

    public static Props getProps(ArbitrageService arbitrageService) {
        return Props.create(ArbitragentActor.class, arbitrageService);
    }

    public ArbitragentActor(ArbitrageService arbitrageService) {
        this.arbitrageService = arbitrageService;
        this.testDepthStamps = new ArrayList<>();
    }

    @Override
    public AbstractActor.Receive createReceive() {
        return receiveBuilder()
                .match(Long.class, timestamp -> processDepthStamps(arbitrageService.removeDepthStamp(timestamp)))
                .match(DepthStamp.class, depthStamp -> processTest(depthStamp))
                .match(GraphNode[][].class, rates -> arbitrage(rates).thenAccept(cycle -> Json.prettyPrint(Json.toJson(cycle))))
                .build();
    }

    private void processTest(DepthStamp depthStamp) {
        testDepthStamps.add(depthStamp);
        if (testDepthStamps.size() == 3) {
            Logger.info("Got em all!");
            List<List<GraphNode>> input = new ArrayList<>();
            testDepthStamps.forEach(stamp -> {
                input.add(stamp.getBids()
                        .stream()
                        .map(bid -> new GraphNode(
                                stamp.getTimestamp(),
                                stamp.getPair(),
                                bid.getPrice(),
                                bid.getQuantity()
                        ))
                        .collect(Collectors.toList()));
                input.add(stamp.getAsks()
                        .stream()
                        .map(ask -> new GraphNode(
                                stamp.getTimestamp(),
                                stamp.getPair().substring(3) + stamp.getPair().substring(0, 3),
                                1.0 / ask.getPrice(),
                                ask.getQuantity()
                        ))
                        .collect(Collectors.toList()));
            });
//            List<List<GraphNode>> organizeInputs = permute(input, 0, new ArrayList<>());
            // TODO: Get these guys and prepare the matrices for arbitrage calculation
            testDepthStamps = new ArrayList<>();
        } else {
            Logger.info("Got some!");
        }
    }

//    private List<List<GraphNode>> permute(List<List<GraphNode>> input, int level, List<List<GraphNode>> output) {
//        input.get(level).forEach(graphNode -> output.addAll(permute(input, level + 1, new ArrayList<>())));
//    }

    private void processDepthStamps(List<DepthStamp> stamps) {
        if (stamps.size() > 0)
            stamps.remove(0);
    }

    private List<GraphNode> decodeCycle(List<Integer> cycle, GraphNode[][] rates) {
        List<GraphNode> out = new ArrayList<>();
        for (int i = 0; i < cycle.size(); i++) {
            if (i == cycle.size() - 1)
                out.add(rates[cycle.get(i)][cycle.get(0)]);
            else
                out.add(rates[cycle.get(i)][cycle.get(i + 1)]);
        }
        return out;
    }

    //  Below is a modification of: https://github.com/elynnyap/algorithms/blob/master/Arbitrage.java by @elynnyap

    private CompletionStage<List<GraphNode>> arbitrage(GraphNode[][] rates) {
        // create new graph with same vertices and edges but modified weights
        int v = rates.length; // number of currencies

        // use bellman ford to detect if there is a negative weight cycle
        double[] distance = new double[v]; // shortest distance from source vertex
        int[] parent = new int[v]; // parent of vertex in shortest-path tree

        // initialize single source
        for (int i = 0; i < v; i++) {
            distance[i] = Double.MAX_VALUE;
            parent[i] = Integer.MAX_VALUE;
        }

        distance[0] = 0; // source vertex - since graph is complete i.e. if a negative cycle exists it can be reached from
        // any vertex, source vertex can be any vertex

        for (int i = 0; i < v - 1; i++) { // relax edges v-1 times
            for (int w = 0; w < v; w++) {
                for (int x = w + 1; x < v; x++) {
                    relax(w, x, rates, distance, parent);
                    relax(x, w, rates, distance, parent); // relax each edge in the graph
                }
            }
        }

        // make a final pass to detect negative weight cycle
        for (int w = 0; w < v; w++) {
            for (int x = w + 1; x < v; x++) {
                if (distance[x] > distance[w] + rates[w][x].getWeight()) {
                    parent[x] = w;
                    return findCycle(x, parent).thenApply(cycle -> decodeCycle(cycle, rates));
                }
                if (distance[w] > distance[x] + rates[x][w].getWeight()) {
                    parent[w] = x;
                    return findCycle(w, parent).thenApply(cycle -> decodeCycle(cycle, rates));
                }
            }
        }

        return CompletableFuture.completedFuture(new ArrayList<>());
    }

    private void relax(int u, int v, GraphNode[][] w, double[] distance, int[] parent) {
        if (distance[v] > distance[u] + w[u][v].getWeight()) {
            distance[v] = distance[u] + w[u][v].getWeight();
            parent[v] = u;
        }
    }

    private CompletionStage<ArrayList<Integer>> findCycle(int u, int[] parent) {

        ArrayList<Integer> cycle = new ArrayList<>();
        int[] count = new int[parent.length]; // count the num of times a vertex is in the path
        u = parent[u];

        while (true) {
            cycle.add(u);
            count[u] += 1;
            if (count[u] > 1) {
                break; // stop when vertex has been encountered on the path twice - indicates cycle
            }
            u = parent[u];
        }

        int v;
        do {
            v = cycle.get(0);
            cycle.remove(0);
        } while (v != u); // modify path to exclude vertices that are not in the cycle

        Collections.reverse(cycle); // reverse path to reflect direction of edges

        Logger.debug("Cycle detected: " + Json.stringify(Json.toJson(cycle)));

        return CompletableFuture.completedFuture(cycle);
    }
}

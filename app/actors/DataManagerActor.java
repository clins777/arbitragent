package actors;

import akka.actor.AbstractActor;
import akka.actor.Props;
import com.fasterxml.jackson.databind.JsonNode;
import models.Ask;
import models.Bid;
import models.DepthStamp;
import play.libs.Json;
import services.ArbitrageService;

import java.util.ArrayList;
import java.util.List;

public class DataManagerActor extends AbstractActor {

    private final ArbitrageService arbitrageService;

    public static Props getProps(ArbitrageService arbitrageService) {
        return Props.create(DataManagerActor.class, arbitrageService);
    }

    public DataManagerActor(ArbitrageService arbitrageService) {
        this.arbitrageService = arbitrageService;
    }

    @Override
    public AbstractActor.Receive createReceive() {
        return receiveBuilder()
                .match(String.class, unparsedDepthStamp -> {
                    DepthStamp depthStamp = parse(unparsedDepthStamp);
                    arbitrageService.saveDepthStamp(depthStamp);
                })
                .build();
    }

    private DepthStamp parse(String unparsedDepthStamp) {
        JsonNode depthStampJson = Json.parse(unparsedDepthStamp);
        DepthStamp depthStamp = new DepthStamp();
        depthStamp.setTimestamp(depthStampJson.get("E").longValue());
        depthStamp.setPair(depthStampJson.get("s").textValue());
        List<Ask> asks = new ArrayList<>();
        for (JsonNode askNode : depthStampJson.get("a")) {
            double quantity = Double.parseDouble(askNode.get(1).textValue());
            if (quantity > 0) {
                double price = Double.parseDouble(askNode.get(0).textValue());
                asks.add(new Ask(price, quantity));
            }
        }
        depthStamp.setAsks(asks);
        List<Bid> bids = new ArrayList<>();
        for (JsonNode bidNode : depthStampJson.get("b")) {
            double quantity = Double.parseDouble(bidNode.get(1).textValue());
            if (quantity > 0) {
                double price = Double.parseDouble(bidNode.get(0).textValue());
                bids.add(new Bid(price, quantity));
            }
        }
        depthStamp.setBids(bids);
        return depthStamp;
    }
}

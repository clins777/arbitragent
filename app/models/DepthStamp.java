package models;

import java.util.List;

public class DepthStamp {

    private long timestamp;
    private String pair;
    private List<Bid> bids;
    private List<Ask> asks;

    public DepthStamp() {
    }

    public DepthStamp(long timestamp, String pair, List<Bid> bids, List<Ask> asks) {
        this.timestamp = timestamp;
        this.pair = pair;
        this.bids = bids;
        this.asks = asks;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getPair() {
        return pair;
    }

    public void setPair(String pair) {
        this.pair = pair;
    }

    public List<Bid> getBids() {
        return bids;
    }

    public void setBids(List<Bid> bids) {
        this.bids = bids;
    }

    public List<Ask> getAsks() {
        return asks;
    }

    public void setAsks(List<Ask> asks) {
        this.asks = asks;
    }
}

package models;

public class GraphNode {

    private long timestamp;
    private String pair;
    private double rate;
    private double weight;
    private double quantity;

    public GraphNode() {
        this.rate = 1.0;
        this.weight = 1.0;
    }

    public GraphNode(long timestamp, String pair, double rate, double quantity) {
        this.timestamp = timestamp;
        this.pair = pair;
        this.rate = rate;
        this.weight = -Math.log(rate);
        this.quantity = quantity;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public String getPair() {
        return pair;
    }

    public void setPair(String pair) {
        this.pair = pair;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }
}

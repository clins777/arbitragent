package services;

import actors.ArbitragentActor;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import models.DepthStamp;
import play.Logger;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

@Singleton
public class ArbitrageService {

    private ConcurrentHashMap<Long, List<DepthStamp>> depthStampMap;
    private final ActorRef arbitragent;

    @Inject
    public ArbitrageService(ActorSystem actorSystem) {
        this.depthStampMap = new ConcurrentHashMap<>();
        this.arbitragent = actorSystem.actorOf(ArbitragentActor.getProps(this));
    }

    public void saveDepthStamp(DepthStamp stamp) {
        long timestamp = stamp.getTimestamp();
        List<DepthStamp> stamps = depthStampMap.containsKey(timestamp) ?
                depthStampMap.get(timestamp) : new ArrayList<>();
        stamps.add(stamp);
        depthStampMap.put(timestamp, stamps);
        if (stamps.size() == 3) {
            Logger.info("Trio collected, sending " + timestamp + " to arbitragent.");
            arbitragent.tell(timestamp, arbitragent);
        } else {
            Logger.info("Collected for " + timestamp + ". Currently has " + stamps.size() + " depth stamps.");
        }
    }

    public List<DepthStamp> removeDepthStamp(long timestamp) {
        Logger.info("Removing " + timestamp + " from table.");
        return depthStampMap.remove(timestamp);
    }
}

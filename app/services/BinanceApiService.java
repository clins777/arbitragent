package services;

import actors.DataManagerActor;
import akka.Done;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.http.javadsl.Http;
import akka.http.javadsl.model.StatusCodes;
import akka.http.javadsl.model.ws.Message;
import akka.http.javadsl.model.ws.WebSocketRequest;
import akka.http.javadsl.model.ws.WebSocketUpgradeResponse;
import akka.japi.Pair;
import akka.stream.ActorMaterializer;
import akka.stream.Materializer;
import akka.stream.javadsl.Flow;
import akka.stream.javadsl.Keep;
import akka.stream.javadsl.Sink;
import akka.stream.javadsl.Source;
import play.Logger;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

@Singleton
public class BinanceApiService {

    // public final String ORDER_BOOK_URL = "https://www.binance.com/api/v1/depth?symbol=<symbol>";
    private final String BNBBTC_DEPTH_ENDPOINT_URL = "wss://stream.binance.com:9443/ws/bnbbtc@depth";
    private final String BNBETH_DEPTH_ENDPOINT_URL = "wss://stream.binance.com:9443/ws/bnbeth@depth";
    private final String ETHBTC_DEPTH_ENDPOINT_URL = "wss://stream.binance.com:9443/ws/ethbtc@depth";
    private final ActorSystem actorSystem;
    private final ArbitrageService arbitrageService;

    @Inject
    public BinanceApiService(ActorSystem actorSystem, ArbitrageService arbitrageService) {
        this.actorSystem = actorSystem;
        this.arbitrageService = arbitrageService;
    }

    public void startAll() {
        start(BNBBTC_DEPTH_ENDPOINT_URL);
        start(BNBETH_DEPTH_ENDPOINT_URL);
        start(ETHBTC_DEPTH_ENDPOINT_URL);
    }

    public void start(String url) {
        Materializer materializer = ActorMaterializer.create(actorSystem);
        Http http = Http.get(actorSystem);
        ActorRef dataManagerActor = actorSystem.actorOf(DataManagerActor.getProps(arbitrageService));
        final Sink<Message, CompletionStage<Done>> printSink =
                Sink.foreach(message -> dataManagerActor.tell(message.asTextMessage().getStrictText(), dataManagerActor));

        final Flow<Message, Message, CompletableFuture<Optional<Message>>> flow =
                Flow.fromSinkAndSourceMat(printSink, Source.maybe(), Keep.right());

        final Pair<CompletionStage<WebSocketUpgradeResponse>, CompletableFuture<Optional<Message>>> pair =
                http.singleWebSocketRequest(WebSocketRequest.create(url), flow, materializer);

        final CompletionStage<Done> connected = pair.first().thenApply(upgrade -> {
            if (upgrade.response().status().equals(StatusCodes.SWITCHING_PROTOCOLS)) {
                return Done.getInstance();
            } else {
                throw new RuntimeException("Connection failed: " + upgrade.response().status());
            }
        });

        connected.thenAccept(done -> Logger.info("Connected"));
    }

    public void stop() {
        actorSystem.terminate();
    }
}

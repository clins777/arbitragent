package controllers;

import actors.ArbitragentActor;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import com.fasterxml.jackson.databind.JsonNode;
import models.Ask;
import models.Bid;
import models.DepthStamp;
import play.libs.ws.WSClient;
import play.mvc.*;
import services.ArbitrageService;
import services.BinanceApiService;

import javax.inject.Inject;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class HomeController extends Controller {

    private BinanceApiService binanceApiService;
    private final WSClient wsClient;
    private final ActorRef arbitragent;

    @Inject
    public HomeController(BinanceApiService binanceApiService, WSClient wsClient, ActorSystem actorSystem, ArbitrageService arbitrageService) {
        this.binanceApiService = binanceApiService;
        this.wsClient = wsClient;
        this.arbitragent = actorSystem.actorOf(ArbitragentActor.getProps(arbitrageService));
    }

    public Result index() {
        return ok(views.html.index.render());
    }

    public Result start() {
        binanceApiService.startAll();
        return ok("Starting to listen");
    }

    public Result stop() {
        binanceApiService.stop();
        return ok("Stopped listening");
    }

    public Result scrape() {
        wsClient.url("https://www.binance.com/api/v1/depth?symbol=BNBBTC&limit=20").get().thenAccept(response -> {
            DepthStamp depthStamp = new DepthStamp();
            depthStamp.setPair("BNBBTC");
            JsonNode responseJson = response.asJson();
            depthStamp.setAsks(StreamSupport.stream(responseJson.get("asks").spliterator(), false)
                    .map(askNode -> (new Ask(
                            Double.parseDouble(askNode.get(0).textValue()),
                            Double.parseDouble(askNode.get(1).textValue()))))
                    .collect(Collectors.toList()));
            depthStamp.setBids(StreamSupport.stream(responseJson.get("bids").spliterator(), false)
                    .map(bidNode -> (new Bid(
                            Double.parseDouble(bidNode.get(0).textValue()),
                            Double.parseDouble(bidNode.get(1).textValue()))))
                    .collect(Collectors.toList()));
            arbitragent.tell(depthStamp, arbitragent);
        });
        wsClient.url("https://www.binance.com/api/v1/depth?symbol=BNBETH&limit=20").get().thenAccept(response -> {
            DepthStamp depthStamp = new DepthStamp();
            depthStamp.setPair("BNBETH");
            JsonNode responseJson = response.asJson();
            depthStamp.setAsks(StreamSupport.stream(responseJson.get("asks").spliterator(), false)
                    .map(askNode -> (new Ask(
                            Double.parseDouble(askNode.get(0).textValue()),
                            Double.parseDouble(askNode.get(1).textValue()))))
                    .collect(Collectors.toList()));
            depthStamp.setBids(StreamSupport.stream(responseJson.get("bids").spliterator(), false)
                    .map(bidNode -> (new Bid(
                            Double.parseDouble(bidNode.get(0).textValue()),
                            Double.parseDouble(bidNode.get(1).textValue()))))
                    .collect(Collectors.toList()));
            arbitragent.tell(depthStamp, arbitragent);
        });
        wsClient.url("https://www.binance.com/api/v1/depth?symbol=ETHBTC&limit=20").get().thenAccept(response -> {
            DepthStamp depthStamp = new DepthStamp();
            depthStamp.setPair("ETHBTC");
            JsonNode responseJson = response.asJson();
            depthStamp.setAsks(StreamSupport.stream(responseJson.get("asks").spliterator(), false)
                    .map(askNode -> (new Ask(
                            Double.parseDouble(askNode.get(0).textValue()),
                            Double.parseDouble(askNode.get(1).textValue()))))
                    .collect(Collectors.toList()));
            depthStamp.setBids(StreamSupport.stream(responseJson.get("bids").spliterator(), false)
                    .map(bidNode -> (new Bid(
                            Double.parseDouble(bidNode.get(0).textValue()),
                            Double.parseDouble(bidNode.get(1).textValue()))))
                    .collect(Collectors.toList()));
            arbitragent.tell(depthStamp, arbitragent);
        });
        return ok();
    }

    public Result test() {
        arbitragent.tell("test", arbitragent);
        return ok("Test sent");
    }
}

name := """arbitragent"""
organization := "com.example"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.12.2"

libraryDependencies ++= Seq(
  guice,
  javaWs,
  "com.googlecode.princeton-java-algorithms" % "algorithms" % "4.0.1"
)

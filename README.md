# Binance Pre Screening

## Mini Program Test (work in progress)

### How to run

You need [sbt](http://www.scala-sbt.org/1.x/docs/Setup.html) installed. Then just run:

> $ sbt run

### GET /start

Starts business logic

### GET /stop

Kills all WebSocket links

### TODO

Calculate arbitrage

## Java Test (work in progress)

Note: Not all these questions are strictly about Java.

**1 - What is the difference between equals() and == ?**

In Java, equals() is a method for functional comparison of two Object whereas == compares the references of these Object. This means that equals() is used to compare the value of objects in a totally customizable way and == is used to tell if both objects are pointing to the same slot in memory or not.

**2 - A Queue class uses the java.lang.Object.notify() method. What other Object method would the Queue class also use?**

**3 - What does this program output?**

```java
// A.java
// ------

class A {
  A() { i = j++ ? ++j : -j; }
  public int i;
  public static int j = 0;
}

// M.java
// ------

class M {
  public static void main(String[] args) {
    A a1 = new A();

    System.out.println(a1.i);
    System.out.println(a2.i);
  }
  
  static A a2 = new A();
}
```

The program won’t compile because of j++ being an integer and the ternary operation i = j++ ? ++j : -j expects a boolean to the left of the question mark. But assuming j++ would be (j++ == 1) instead then:

- j is static (same value in memory on all instances of A, starts at 0)
- on the main class (M) a static reference of A (a2) executes the following:
	- ```j++ == 1``` which is ```0 == 1``` // after this operation j becomes 1 because of the post increment ++
	- ```i = -j``` >> ```i = -1```
	- ```a2 = {i=-1, j=1}```
- then a1 is instantiated:
	- ```j++ == 1``` >> ```1 == 1``` // after this operation j becomes 2
	- ```i = ++j``` >> ```i = 3```
	- ```a1 = {i=3, j=3}```
	- ```a2 = {i=-1, j=3}```
- so the output is 3 and then -1

**4 - Implement int FindNextPrime(int i), that returns the next prime number greater than i**

```java
Map<Integer, Boolean> primeMap = new HashMap<>();

int FindNextPrime(int i) {

    int number = i + 1;

    if (primeMap.containsKey(number))
        return primeMap.get(number) ? number : FindNextPrime(number);

    boolean isPrime = true;

    while (i > 1 && isPrime) {
        isPrime = (number % i) != 0;
        i--;
    }

    primeMap.put(number, isPrime);

    return isPrime ? number : FindNextPrime(number);
}
```


**5 - Describe briefly how you would debug a deadlocked server process using a debugger you are familiar with.**



**6 - Describe briefly how you would choose between HashTable, HashMap and ConcurrentHashMap?**

